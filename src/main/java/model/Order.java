package model;
import java.util.*;
public class Order {
	private int id;
	private int idProdus;
	private int idCustomer;
	private int cantitate;
	
	public Order(int id, int idProdus,int idCustomer,int cantitate) {
		this.id=id;
		this.idProdus=idProdus;
		this.idCustomer=idCustomer;
		this.cantitate=cantitate;
	}
	
	public int getId() {
		return id;
	}
	public int getProdus(){
		return idProdus;
	}
	public int getCostumer() {
		return idCustomer;
	}
	public int getCantitate() {
		return cantitate;
	}
}
