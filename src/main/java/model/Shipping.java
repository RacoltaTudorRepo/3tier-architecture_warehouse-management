package model;

public class Shipping {
	private int idOrder;
	private String adress;
	private int days;
	public Shipping(int idOrder,String adress,int days) {
		this.idOrder=idOrder;
		this.adress=adress;
		this.days=days;
	}
	public int getId() {
		return idOrder;
	}
	public String getAdress() {
		return adress;
	}
	public int getDays() {
		return days;
	}
}
