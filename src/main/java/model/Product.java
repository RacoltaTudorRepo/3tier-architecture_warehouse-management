package model;

public class Product {
	private int id;
	private String nume;
	private int cantitate;
	public Product(int id,String nume,int cantitate) {
		this.nume=nume;
		this.id=id;
		this.cantitate=cantitate;
	}
	public String getNume() {
		return nume;
	}
	public int getCantitate() {
		return cantitate;
	}
	public int getID() {
		return id;
	}
}
