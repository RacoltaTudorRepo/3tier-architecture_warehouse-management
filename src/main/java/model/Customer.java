package model;
public class Customer {
	private String nume;
	private String adresa;
	private int id;
	public Customer(String nume,String adresa,int id) {
		this.nume=nume;
		this.adresa=adresa;
		this.id=id;
	}
	public String getNume() {
		return nume;
	}
	public String getAdress() {
		return adresa;
	}
	public int getId() {
		return id;
	}
}
