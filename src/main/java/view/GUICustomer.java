package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import model.*;

import javax.swing.JButton;
import javax.swing.*;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
/**Interfata grafice pentru lucrul cu obiecte de tip Customer*/
public class GUICustomer {
	private Object[][] data;
	private String[] columns= new String[3];
	private JFrame frame;
	private JTextField textField;
	private JTable table;
	private DefaultTableModel model;
	private CustomerDAO dao;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	
		/**Conctructorul in care vom efectua prin action listeneri diferite operatii */
		public GUICustomer() throws ClassNotFoundException {
			
		initHeader();	
		frame = new JFrame("Customer application");
		frame.setBounds(100, 100, 573, 416);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		textField = new JTextField(); //INSERT INFO
		textField.setBounds(28, 47, 191, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		
		//INSERT
		JButton btnInsert = new JButton("Insert");
		btnInsert.setBounds(76, 78, 89, 23);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setRowCount(0);
				String aux=textField.getText();
				String[] splitat=aux.split(",");
				Customer client= new Customer(splitat[0],splitat[1],Integer.parseInt(splitat[2]));
				Object rowdata[]=new Object[3];
				ArrayList<Customer> clients=new ArrayList<Customer>();
				try {
					dao.insertObject(client);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					clients = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Customer c: clients) {
					rowdata[0]=new String(c.getNume());
					rowdata[1]=new String(c.getAdress());
					rowdata[2]=new String(Integer.toString(c.getId()));
					model.addRow(rowdata);
				}
			}
		});
		frame.getContentPane().add(btnInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(83, 183, 252, 160);
		frame.getContentPane().add(scrollPane);
		
		
		table = new JTable();
		model = new DefaultTableModel(data,columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		textField_1 = new JTextField();//ID UPDATE
		textField_1.setBounds(274, 47, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();//NEW INFO
		textField_2.setBounds(370, 47, 157, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		//UPDATE
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				int id=Integer.parseInt(textField_1.getText());
				String aux=textField_2.getText();
				String[] splitat=aux.split(",");
				Customer client= new Customer(splitat[0],splitat[1],Integer.parseInt(splitat[2]));
				Object rowdata[]=new Object[3];
				ArrayList<Customer> clients=new ArrayList<Customer>();
				try {
					dao.update(client);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					clients = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Customer c: clients) {
					rowdata[0]=new String(c.getNume());
					rowdata[1]=new String(c.getAdress());
					rowdata[2]=new String(Integer.toString(c.getId()));
					model.addRow(rowdata);
				}
			}
		});
		btnUpdate.setBounds(406, 78, 89, 23);
		frame.getContentPane().add(btnUpdate);
		
		JLabel lblId = new JLabel("Customer ID");
		lblId.setBounds(284, 28, 121, 14);
		frame.getContentPane().add(lblId);
		
		JLabel lblNewInfo = new JLabel("New info");
		lblNewInfo.setBounds(415, 28, 112, 14);
		frame.getContentPane().add(lblNewInfo);
		
		JLabel lblNewClient = new JLabel("New client");
		lblNewClient.setBounds(92, 22, 89, 14);
		frame.getContentPane().add(lblNewClient);
		
		textField_3 = new JTextField();//ID DELETE
		textField_3.setBounds(274, 122, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		//DELETE
		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				int id=Integer.parseInt(textField_3.getText());
				Customer client=new Customer(" "," ",id);
				Object rowdata[]=new Object[3];
				ArrayList<Customer> clients=new ArrayList<Customer>();
				try {
					dao.deleteObject(client);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					clients = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Customer c: clients) {
					rowdata[0]=new String(c.getNume());
					rowdata[1]=new String(c.getAdress());
					rowdata[2]=new String(Integer.toString(c.getId()));
					model.addRow(rowdata);
				}
				model.fireTableDataChanged();
			}
		});
		btnNewButton.setBounds(394, 121, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblCustomerId = new JLabel("Customer ID");
		lblCustomerId.setBounds(284, 100, 112, 14);
		frame.getContentPane().add(lblCustomerId);
		
		//REFRESH
				JButton refreshButton = new JButton("Refresh");
				refreshButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						model.setRowCount(0);
						Object rowdata[]=new Object[3];
						ArrayList<Customer> products=new ArrayList<Customer>();
						try {
							products = dao.selectAll();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						for(Customer c: products) {
							rowdata[0]=new String(c.getNume());
							rowdata[1]=new String(c.getAdress());
							rowdata[2]=new String(Integer.toString(c.getId()));
							model.addRow(rowdata);
						}
					}
				});
				refreshButton.setBounds(394, 150, 89, 23);
				frame.getContentPane().add(refreshButton);
		
		
	}
		/**Initializare header prin reflection a unui obiect de tip Customer*/	
	public void initHeader() { //REFLECTION
		Customer cust=new Customer("","",0);
		Class cls=cust.getClass();
		Field fieldlist[]= cls.getDeclaredFields();
		int i=0;
		for(Field field:fieldlist) {
			columns[i]=new String(field.getName());
			i++;
		}
	}
	/**Metoda pentru a returna frame-ul*/
	public JFrame getFrame() {
		return frame;
	}
	/**Metoda pentru setarea unui database access object e tip CustomerDAO*/
	public void setDao(CustomerDAO dao) {
		this.dao=dao;
	}
}
