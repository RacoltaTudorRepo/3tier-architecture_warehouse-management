package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import dao.ProductDAO;
import model.*;

import javax.swing.JButton;
import javax.swing.*;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class GUIProduct {
	private Object[][] data;
	private String[] columns= new String[3];
	private JFrame frame;
	private JTextField textField;
	private JTable table;
	private DefaultTableModel model;
	private ProductDAO dao;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	

		public GUIProduct() throws ClassNotFoundException {
			
		initHeader();	
		frame = new JFrame("Product application");
		frame.setBounds(100, 100, 573, 416);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		textField = new JTextField(); //INSERT INFO
		textField.setBounds(28, 47, 191, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		
		//INSERT
		JButton btnInsert = new JButton("Insert");
		btnInsert.setBounds(76, 78, 89, 23);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setRowCount(0);
				String aux=textField.getText();
				String[] splitat=aux.split(",");
				Product produs= new Product(Integer.parseInt(splitat[0]),splitat[1],Integer.parseInt(splitat[2]));
				Object rowdata[]=new Object[3];
				ArrayList<Product> products=new ArrayList<Product>();
				try {
					dao.insertObject(produs);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					products = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Product c: products) {
					rowdata[0]=new String(Integer.toString(c.getID()));
					rowdata[1]=new String(c.getNume());
					rowdata[2]=new String(Integer.toString(c.getCantitate()));
					model.addRow(rowdata);
				}
			}
		});
		frame.getContentPane().add(btnInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(83, 183, 252, 160);
		frame.getContentPane().add(scrollPane);
		
		
		table = new JTable();
		model = new DefaultTableModel(data,columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		textField_1 = new JTextField();//ID UPDATE
		textField_1.setBounds(274, 47, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();//NEW INFO
		textField_2.setBounds(370, 47, 157, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		//UPDATE
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				int id=Integer.parseInt(textField_1.getText());
				String aux=textField_2.getText();
				String[] splitat=aux.split(",");
				Product produs= new Product(Integer.parseInt(splitat[0]),splitat[1],Integer.parseInt(splitat[2]));
				Object rowdata[]=new Object[3];
				ArrayList<Product> products=new ArrayList<Product>();
				try {
					dao.update(produs);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					products = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Product c: products) {
					rowdata[0]=new String(Integer.toString(c.getID()));
					rowdata[1]=new String(c.getNume());
					rowdata[2]=new String(Integer.toString(c.getCantitate()));
					model.addRow(rowdata);
				}
			}
		});
		btnUpdate.setBounds(406, 78, 89, 23);
		frame.getContentPane().add(btnUpdate);
		
		JLabel lblId = new JLabel("Product ID");
		lblId.setBounds(284, 28, 121, 14);
		frame.getContentPane().add(lblId);
		
		JLabel lblNewInfo = new JLabel("New info");
		lblNewInfo.setBounds(415, 28, 112, 14);
		frame.getContentPane().add(lblNewInfo);
		
		JLabel lblNewClient = new JLabel("New product");
		lblNewClient.setBounds(92, 22, 89, 14);
		frame.getContentPane().add(lblNewClient);
		
		textField_3 = new JTextField();//ID DELETE
		textField_3.setBounds(274, 122, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		//DELETE
		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				int id=Integer.parseInt(textField_3.getText());
				Product produs=new Product(id," ",0);
				Object rowdata[]=new Object[3];
				ArrayList<Product> products=new ArrayList<Product>();
				try {
					dao.deleteObject(produs);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					products = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Product c: products) {
					rowdata[0]=new String(Integer.toString(c.getID()));
					rowdata[1]=new String(c.getNume());
					rowdata[2]=new String(Integer.toString(c.getCantitate()));
					model.addRow(rowdata);
				}
			}
		});
		btnNewButton.setBounds(394, 121, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblCustomerId = new JLabel("Product ID");
		lblCustomerId.setBounds(284, 100, 112, 14);
		frame.getContentPane().add(lblCustomerId);
		
		//REFRESH
		JButton refreshButton = new JButton("Refresh");
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				Object rowdata[]=new Object[3];
				ArrayList<Product> products=new ArrayList<Product>();
				try {
					products = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Product c: products) {
					rowdata[0]=new String(Integer.toString(c.getID()));
					rowdata[1]=new String(c.getNume());
					rowdata[2]=new String(Integer.toString(c.getCantitate()));
					model.addRow(rowdata);
				}
			}
		});
		refreshButton.setBounds(394, 150, 89, 23);
		frame.getContentPane().add(refreshButton);
		
	}
		
	public void initHeader() { //REFLECTION
		Product cust=new Product(0,"",0);
		Class cls=cust.getClass();
		Field fieldlist[]= cls.getDeclaredFields();
		int i=0;
		for(Field field:fieldlist) {
			columns[i]=new String(field.getName());
			i++;
		}
	}
	public JFrame getFrame() {
		return frame;
	}
	public void setDao(ProductDAO dao) {
		this.dao=dao;
	}
}
