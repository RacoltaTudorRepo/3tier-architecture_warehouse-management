package view;

import java.awt.EventQueue;

import javax.swing.table.DefaultTableModel;

import dao.OrderDAO;
import dao.ProductDAO;
import model.*;

import javax.swing.*;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
/**Interfata de utilizator pentru lucrul cu obiecte Order si Shipping*/
public class GUIOrder {
	private Object[][] data;
	private String[] columns= new String[4];
	private String[] columns2= new String[3];
	private JFrame frame;
	private JTextField textField;
	private JTable table;
	private DefaultTableModel model;
	private JTable table1;
	private DefaultTableModel model1;
	private OrderDAO dao;

	/**Conctructorul in care vom efectua prin action listeneri diferite operatii */
	public GUIOrder() throws ClassNotFoundException {

		initHeader();	
		frame = new JFrame("Order application");
		frame.setBounds(100, 100, 573, 416);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewClient = new JLabel("New order");
		lblNewClient.setBounds(92, 22, 89, 14);
		frame.getContentPane().add(lblNewClient);

		textField = new JTextField(); //INSERT INFO
		textField.setBounds(28, 47, 191, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);


		//INSERT
		JButton btnInsert = new JButton("Insert");
		btnInsert.setBounds(76, 78, 89, 23);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setRowCount(0);
				String aux=textField.getText();
				String[] splitat=aux.split(",");
				Order order= new Order(Integer.parseInt(splitat[0]),Integer.parseInt(splitat[1]),Integer.parseInt(splitat[2]),Integer.parseInt(splitat[3]));
				Object rowdata[]=new Object[4];
				ArrayList<Order> orders=new ArrayList<Order>();
				try {
					if(dao.insert(order)==1) {//CREATE BILL
						Product produs=dao.createProduct(order);
						Customer client=dao.createCustomer(order);
						String filetowrite = "C:\\Users\\Tudor\\Desktop\\Chitanta.txt"; 
						FileWriter fw = new FileWriter(filetowrite);
						String printare="Comanda: "+order.getId();
						printare+="\nDate client: "+client.getNume();
						printare+="\nDate produs: "+produs.getNume();
						printare+="\nCantitate: " +produs.getCantitate();
						fw.write(printare);
						fw.close();
						//INSERT IN SHIPPING
						dao.insertShipping(order.getId(), client.getAdress());



					}
					orders = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Order c: orders) {
					rowdata[0]=new String(Integer.toString(c.getId()));
					rowdata[1]=new String(Integer.toString(c.getProdus()));
					rowdata[2]=new String(Integer.toString(c.getCostumer()));
					rowdata[3]=new String(Integer.toString(c.getCantitate()));
					model.addRow(rowdata);
				}

			}
		});
		frame.getContentPane().add(btnInsert);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(50, 183, 230, 160);
		frame.getContentPane().add(scrollPane);


		table = new JTable();
		model = new DefaultTableModel(data,columns);
		table.setModel(model);
		scrollPane.setViewportView(table);

		//REFRESH
		JButton refreshButton = new JButton("Refresh");
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				Object rowdata[]=new Object[4];
				ArrayList<Order> products=new ArrayList<Order>();
				try {
					products = dao.selectAll();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Order c: products) {
					rowdata[0]=new String(Integer.toString(c.getId()));
					rowdata[1]=new String(Integer.toString(c.getProdus()));
					rowdata[2]=new String(Integer.toString(c.getCostumer()));
					rowdata[3]=new String(Integer.toString(c.getCantitate()));
					model.addRow(rowdata);
				}
			}
		});
		refreshButton.setBounds(150, 150, 89, 23);
		frame.getContentPane().add(refreshButton);


		//SHIPPINGS
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(320, 183, 200, 160);
		frame.getContentPane().add(scrollPane1);


		table1 = new JTable();
		model1 = new DefaultTableModel(data,columns2);
		table1.setModel(model1);
		scrollPane1.setViewportView(table1);

		//REFRESH
		JButton refreshButton1 = new JButton("Refresh");
		refreshButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model1.setRowCount(0);
				Object rowdata[]=new Object[3];
				ArrayList<Shipping> shipping=new ArrayList<Shipping>();
				try {
					shipping = dao.selectAlls();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for(Shipping c: shipping) {
					rowdata[0]=new String(Integer.toString(c.getId()));
					rowdata[1]=new String(c.getAdress());
					rowdata[2]=new String(Integer.toString(c.getDays()));
					model1.addRow(rowdata);
				}
			}
		});
		refreshButton1.setBounds(350, 150, 89, 23);
		frame.getContentPane().add(refreshButton1);		

	}

	/**Initializare header prin reflection a unui obiect de tip Order*/	
	public void initHeader() { //REFLECTION
		Order cust=new Order(0,0,0,0);
		Class cls=cust.getClass();
		Field fieldlist[]= cls.getDeclaredFields();
		int i=0;
		for(Field field:fieldlist) {
			columns[i]=new String(field.getName());
			i++;
		}

		Shipping ship=new Shipping(0," ",0);
		Class cls1=ship.getClass();
		Field fieldlist1[]= cls1.getDeclaredFields();
		i=0;
		for(Field field:fieldlist1) {
			columns2[i]=new String(field.getName());
			i++;
		}
	}
	/**Metoda pentru a returna frame-ul*/
	public JFrame getFrame() {
		return frame;
	}
	/**Metoda pentru setarea unui database access object e tip OrderDAO*/
	public void setDao(OrderDAO dao) {
		this.dao=dao;
		dao.setFrame(frame);
	}
}
