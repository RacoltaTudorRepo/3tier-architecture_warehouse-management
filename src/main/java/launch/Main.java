package launch;

import java.lang.reflect.Field;
import java.sql.*;

import connection.MyConnection;
import dao.CustomerDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import view.GUICustomer;
import view.GUIOrder;
import view.GUIProduct;
import model.*;
/**Clasa pentru lansarea aplicatiei*/
public class Main {
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Connection connection=MyConnection.getConnection();
		
		GUICustomer gui=new GUICustomer();
		CustomerDAO dao=new CustomerDAO();
		dao.setConnection(connection);
		gui.setDao(dao);
		
		GUIProduct gui1=new GUIProduct();
		ProductDAO dao1=new ProductDAO();
		dao1.setConnection(connection);
		gui1.setDao(dao1);
		
		GUIOrder gui2=new GUIOrder();
		OrderDAO dao2=new OrderDAO();
		dao2.setConnection(connection);
		gui2.setDao(dao2);
		
		gui.getFrame().setVisible(true);
		gui1.getFrame().setVisible(true);
		gui2.getFrame().setVisible(true);
	}
}
