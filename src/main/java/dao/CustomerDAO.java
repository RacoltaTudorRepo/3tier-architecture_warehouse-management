package dao;
import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import model.Customer;
/**Data access object pentru operatii de tip Customer*/
public class CustomerDAO extends AbstractDao<Customer> {
	/**Metoda pentru inserarea unui obiect de tip Customer
	 */
	public void insertObject(Customer obj) throws SQLException {
		PreparedStatement insert=null;
		String insertStatementString = "INSERT INTO Customer (nume,adresa,id)"
					+ " VALUES (?,?,?)";
		insert=connection.prepareStatement(insertStatementString);
		insert.setString(1, obj.getNume());
		insert.setString(2, obj.getAdress());
		insert.setInt(3, obj.getId());
		insert.executeUpdate();
		}
	/**Metoda pentru stergerea unui obiect de tip Customer
	 */
	public void deleteObject(Customer obj) throws SQLException{
		PreparedStatement delete=null;
		String insertStatementString = "DELETE FROM Customer WHERE ID=?";	
		delete=connection.prepareStatement(insertStatementString);
		delete.setInt(1, obj.getId());
		delete.executeUpdate();
	}
	/**Metoda pentru selectarea tuturor obiectelor de tip Customer
	 */
	public ArrayList<Customer> selectAll() throws SQLException {
		ArrayList <Customer> clienti=new ArrayList<Customer>();
		String selectString = "SELECT * FROM CUSTOMER";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		rs=statement.executeQuery();
		while(rs.next())
			clienti.add(new Customer(rs.getString(1),rs.getString(2),rs.getInt(3)));
		return clienti;
	}
	/**Metoda pentru updatarea unui obiect de tip Customer
	 */
	public void update(Customer obj) throws SQLException{
		String querry = "UPDATE customer SET nume=?, adresa=? WHERE id = ?";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setString(1, obj.getNume());
        statement.setString(2, obj.getAdress());
        statement.setInt(3, obj.getId());
        statement.executeUpdate();
	}
}