package dao;

import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import connection.MyConnection;
import model.Customer;
import model.Order;
import model.Product;
/**Clasa abstracta in care vom tine conexiunea
 */
public class AbstractDao<T> {
	private final Class<T> type;
	/**Conexiunea cu baza de date*/
	protected Connection connection;
	public AbstractDao() {
		this.type=(Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	/**Setare conexiune*/
	public void setConnection(Connection connection) {
		this.connection=connection;
	}
}
