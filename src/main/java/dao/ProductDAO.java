package dao;
import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import model.Product;
/**Data access object pentru operatii de tip product*/
public class ProductDAO extends AbstractDao<Product> {
	/**Metoda pentru inserarea unui obiect de tip Product*/
	public void insertObject(Product obj) throws SQLException {
		PreparedStatement insert=null;
		String insertStatementString = "INSERT INTO Product (id,nume,cantitate)"
					+ " VALUES (?,?,?)";
		insert=connection.prepareStatement(insertStatementString);
		insert.setInt(1, obj.getID());
		insert.setString(2, obj.getNume());
		insert.setInt(3, obj.getCantitate());
		insert.executeUpdate();
		}
	/**Metoda pentru stergerea unui obiect de tip Product*/
	public void deleteObject(Product obj) throws SQLException{
		PreparedStatement delete=null;
		String insertStatementString = "DELETE FROM Product WHERE ID=?";	
		delete=connection.prepareStatement(insertStatementString);
		delete.setInt(1, obj.getID());
		delete.executeUpdate();
	}
	/**Metoda pentru selectarea tuturor elementelor de tip Product*/
	public ArrayList<Product> selectAll() throws SQLException {
		ArrayList <Product> produse=new ArrayList<Product>();
		String selectString = "SELECT * FROM PRODUCT";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		rs=statement.executeQuery();
		while(rs.next())
			produse.add(new Product(rs.getInt(1),rs.getString(2),rs.getInt(3)));
		return produse;
	}
	/**Metoda pentru updatarea unui obiect de tip Product*/
	public void update(Product obj) throws SQLException{
		String querry = "UPDATE product SET nume=?, cantitate=? WHERE id = ?";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setString(1, obj.getNume());
        statement.setInt(2, obj.getCantitate());
        statement.setInt(3, obj.getID());
        statement.executeUpdate();
	}
}