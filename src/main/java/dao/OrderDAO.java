package dao;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;

import model.*;

/**Data access object pentru operatii de tip order*/
public class OrderDAO extends AbstractDao<Order>{
	private JFrame frame;
	/**Metoda pentru inserarea unui obiect de tip Order
	 */
	public int insert(Order order) throws SQLException {
		Product produs=null;
		String selectString = "SELECT * FROM Product WHERE id=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setInt(1, order.getProdus());
		rs=statement.executeQuery();
		if(rs.next())
			produs=new Product(rs.getInt(1),rs.getString(2),rs.getInt(3));
		if(order.getCantitate()>produs.getCantitate())
			{JOptionPane.showMessageDialog(frame,"Cantitate insuficienta,nu se poate satisface comanda!");
			return -1;
			}
		else {
			//INSERT
			PreparedStatement insert=null;
			String insertStatementString = "INSERT INTO Orders (id,idProdus,idClient,cantitate)"
						+ " VALUES (?,?,?,?)";
			insert=connection.prepareStatement(insertStatementString);
			insert.setInt(1, order.getId());
			insert.setInt(2, order.getProdus());
			insert.setInt(3, order.getCostumer());
			insert.setInt(4, order.getCantitate());
			insert.executeUpdate();
			
			//UPDATE
			String querry = "UPDATE product SET nume=?, cantitate=? WHERE id = ?";
	        PreparedStatement statement1 = null;
	        statement = connection.prepareStatement(querry);
	        statement.setString(1, produs.getNume());
	        statement.setInt(2, produs.getCantitate()-order.getCantitate());
	        statement.setInt(3, produs.getID());
	        statement.executeUpdate();
	        return 1;
		}
	}
	/**Metoda pentru selectarea tuturor obiectelor de tip Order
	 */
	public ArrayList<Order> selectAll() throws SQLException {
		ArrayList <Order> orders=new ArrayList<Order>();
		String selectString = "SELECT * FROM ORDERS";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		rs=statement.executeQuery();
		while(rs.next())
			orders.add(new Order(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4)));
		return orders;
	}
	/**Metoda pentru crearea unui obiect de tip Product pe baza orderId
	 */
	public Product createProduct(Order order) throws SQLException {
		Product produs=null;
		String selectString = "SELECT * FROM Product WHERE id=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setInt(1, order.getProdus());
		rs=statement.executeQuery();
		if(rs.next())
			produs=new Product(rs.getInt(1),rs.getString(2),order.getCantitate());
		return produs;
	}
	/**Metoda pentru crearea unui obiect de tip Customer pe baza orderId
	 */
	public Customer createCustomer(Order order) throws SQLException {
		Customer client=null;
		String selectString = "SELECT * FROM Customer WHERE id=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setInt(1, order.getCostumer());
		rs=statement.executeQuery();
		if(rs.next())
			client=new Customer(rs.getString(1),rs.getString(2),order.getCostumer());
		return client;
	}
	/**Metoda pentru inserarea unui obiect de tip Shipping
	 */
	public void insertShipping(int orderId, String adress) throws SQLException {
		Random rand=new Random();
		PreparedStatement insert=null;
		String insertStatementString = "INSERT INTO Shipping (idOrder,adresa,zile)"
					+ " VALUES (?,?,?)";
		insert=connection.prepareStatement(insertStatementString);
		insert.setInt(1, orderId);
		insert.setString(2, adress);
		int zile=rand.nextInt(10);
		insert.setInt(3,zile);
		insert.executeUpdate();
		JOptionPane.showMessageDialog(frame,"Livrare inregistrata pentru "+adress+" in aproximativ "+zile+" zile");
		
	}
	/**Metoda pentru selectarea tuturor inregistrarilor din Shipping
	 */
	public ArrayList<Shipping> selectAlls() throws SQLException{
		ArrayList <Shipping> shippings=new ArrayList<Shipping>();
		String selectString = "SELECT * FROM SHIPPING";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		rs=statement.executeQuery();
		while(rs.next())
			shippings.add(new Shipping(rs.getInt(1),rs.getString(2),rs.getInt(3)));
		return shippings;
	}
	/**Metoda de setare frame in vederea afisarii JOptionPane
	 */
	public void setFrame(JFrame frame) {
		this.frame=frame;
	}
}
